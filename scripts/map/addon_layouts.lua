local AllLayouts = require("map/layouts").Layouts

--Do this beforehand because I'm too lazy to figure a "proper way" if it's not needed. -M
AllLayouts.MoonbaseEcho = deepcopy(AllLayouts.MoonbaseOne)
AllLayouts.MoonbaseEcho.ground_types = {
	WORLD_TILES.BEACH, WORLD_TILES.BEACH, WORLD_TILES.BEACH, WORLD_TILES.BEACH, WORLD_TILES.BEACH,
	WORLD_TILES.MAGMAFIELD, WORLD_TILES.MAGMAFIELD, --tiletype 6/7 would be grass/forest, now it's magmafield
}

AllLayouts.MoonbaseEcho.layout.palmtree_burnt = AllLayouts.MoonbaseEcho.layout.evergreen
AllLayouts.MoonbaseEcho.layout.evergreen = nil

AllLayouts.DragonflyArenaVolcanic = deepcopy(AllLayouts.DragonflyArena)
AllLayouts.DragonflyArenaVolcanic.ground_types = {
	WORLD_TILES.VOLCANO, WORLD_TILES.VOLCANO, WORLD_TILES.VOLCANO, WORLD_TILES.VOLCANO, WORLD_TILES.VOLCANO,
}
AllLayouts.DragonflyArenaVolcanic.layout.volcano_shrub = AllLayouts.DragonflyArenaVolcanic.layout.marsh_tree
AllLayouts.DragonflyArenaVolcanic.layout.marsh_tree = nil