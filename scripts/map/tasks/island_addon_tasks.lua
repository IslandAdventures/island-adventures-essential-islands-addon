AddTask("DragonflyDesert", {
    locks = LOCKS.ISLAND2,
    keys_given = {KEYS.ISLAND3},
    crosslink_factor = math.random(0,1),
    make_loop = math.random(0, 100) < 50,
    room_choices = {
        ["DragonflyArena"] = 1,
        ["Badlands"] = math.random(1,2),
    },
    room_bg = WORLD_TILES.DIRT,
    background_room = "BGBadlands",
    colour = {r = 1, g = 0.6, b = 1, a = 1},
})

AddTask("OasisDesert", {
    locks = LOCKS.ISLAND2,
    keys_given = {KEYS.ISLAND3},
    crosslink_factor = math.random(0, 1),
    make_loop = math.random(0, 100) < 50,
    room_choices = {
        ["LightningBluffAntlion"] = 1,
        ["LightningBluffOasis"] = 1,
    },
    room_bg = WORLD_TILES.DIRT,
    background_room = "BGLightningBluff",
    colour = {r = .05, g = .5, b = .05, a = 1},
})

AddTask("BananaMeadow", {
    locks = LOCKS.ISLAND2,
    keys_given = {KEYS.ISLAND3},
    crosslink_factor = math.random(0, 1),
    make_loop = math.random(0, 100) < 50,
    room_choices = {
        ["MeadowBananas"] = 2,
        ["MeadowGlommer"] = 1,
    },
    room_bg = WORLD_TILES.MEADOW,
    colour = {r = 1, g = 1, b = 0, a = 1},
})

AddTask("WalrusMagma", {
    locks = LOCKS.NONE,
    keys_given = {KEYS.ISLAND2},
    crosslink_factor = math.random(0, 1),
    make_loop = math.random(0, 100) < 50,
    room_choices = {
        ["MagmaWalrus"] = 1,
        ["MagmaMoonstone"] = 1,
        ["MagmaTallBird"] = 1,
    },
    room_bg = WORLD_TILES.MAGMAFIELD,
    colour = {r = 0, g = 0, b = 1, a = 1},
})

AddTask("MoonQuayBeach", {
    locks = LOCKS.ISLAND2,
    keys_given = {KEYS.ISLAND3},
    crosslink_factor = math.random(0, 1),
    make_loop = math.random(0, 100) < 50,
    room_choices = {
        ["PalmconeBeach"] = 3,
    },
    room_bg = WORLD_TILES.MONKEY_GROUND,
    colour = {r = 1, g = 1, b = 0, a = 1}
})