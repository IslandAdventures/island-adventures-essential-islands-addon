AddRoom("PalmconeBeach", {
	colour = {r = .5, g = 0.6, b = .080, a = .10},
	value = WORLD_TILES.MONKEY_GROUND,
	contents = {
	    distributepercent = .4,
	    distributeprefabs = {
			palmconetree = 0.75,
			bananabush = 0.25,
			monkeytail = 0.25,
            seashell_beached = 0.2,
			lightcrab = 0.15,
		},
	}
})