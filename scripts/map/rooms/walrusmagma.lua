AddRoom("MagmaWalrus", {
    colour = {r = .55, g = .75, b = .75, a = .50},
    value = WORLD_TILES.MAGMAFIELD,
    contents = {
        countprefabs = {
            walrus_camp = 3
        },
        distributepercent = .3,
        distributeprefabs = {
            magmarock = 1,
            magmarock_gold = 0.75,
            rock1 = 0.5,
            rock2 = 0.3,
            rocks = 1,
            rock_flintless = 1,
            sapling =  .75,
        },
    }
})

AddRoom("MagmaMoonstone", {
    colour = {r = .55, g = .75, b = .75, a = .50},
    value = WORLD_TILES.MAGMAFIELD,
    contents = {
        countstaticlayouts = {
            ["MoonbaseEcho"] = 1
        },
        distributepercent = .3,
        distributeprefabs = {
            magmarock = 1,
            magmarock_gold = 0.75,
            rock1 = 0.5,
            rock2 = 0.3,
            rocks = 1,
            rock_flintless = 1,
            sapling =  .75,
        },
    }
})