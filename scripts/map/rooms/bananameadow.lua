AddRoom("MeadowBananas", {
    colour = {r = .8, g = .4, b = .4, a = .50},
    value = WORLD_TILES.MEADOW,
    contents =  {
        distributepercent = .4,
        distributeprefabs = {
			grass = 1.5,
            flower = 0.5,
            bananabush = 0.75,
			sweet_potato_planted = 0.75,
            beehive = 0.1,
            palmtree = 0.75,
            rock_flintless = 0.1,
        },
    }
})

AddRoom("MeadowGlommer", {
    colour = {r = .8, g = .4, b = .4, a = .50},
    value = WORLD_TILES.MEADOW,
    contents =  {
        countprefabs = {
			statueglommer = 1,
		},
        distributepercent = .4,
        distributeprefabs = {
			grass = 1.5,
            flower = 0.5,
            bananabush = 0.75,
			sweet_potato_planted = 0.75,
            beehive = 0.1,
            palmtree = 0.75,
            rock_flintless = 0.1,
        },
    }
})