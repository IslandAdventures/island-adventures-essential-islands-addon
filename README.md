# Essential Islands
for Island Adventures
------------------------

This is an add-on for the Island Adventures mod for the game Don't Starve Together.

It adds special Islands and loot drops to integrate basegame content into Island Adventures.

[Available on Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1780226102)
