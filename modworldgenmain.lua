local AddonPrefabs = {
    "lava_pond", "bananabush", "palmconetree", "monkeytail", "houndbone", "oasis_cactus", "cactus", "walrus_camp",
}

for i = 1, #AddonPrefabs, 1 do
       GLOBAL.SpawnUtil.AddLandCommonSpawn(AddonPrefabs[i])
end

local modname = modname
local modimport = modimport
local GetModConfigData = GetModConfigData
local AddTaskPreInit = AddTaskPreInit
local AddTaskSetPreInit = AddTaskSetPreInit
GLOBAL.setfenv(1, GLOBAL)

-- Import dependencies.
package.loaded["librarymanager"] = nil
local AutoSubscribeAndEnableWorkshopMods = require("librarymanager")
if IsWorkshopMod(modname) then
    AutoSubscribeAndEnableWorkshopMods({"workshop-1467214795"})
else  --if the Gitlab Versions dont exist fallback on workshop version
    AutoSubscribeAndEnableWorkshopMods({KnownModIndex:GetModActualName(" Island Adventures - GitLab Ver.") or "workshop-1467214795"})
end

require("map/addon_layouts")
modimport("scripts/map/rooms/walrusmagma")
modimport("scripts/map/rooms/bananameadow")
modimport("scripts/map/rooms/glommerbeach")
modimport("scripts/map/tasks/island_addon_tasks")

AddTaskSetPreInit("shipwrecked", function(task_set)
    if GetModConfigData("walrusmagma") then
        table.insert(task_set.tasks, "WalrusMagma")
    end

    if GetModConfigData("dragonflydesert") then
        table.insert(task_set.tasks, "DragonflyDesert")
    end

    if GetModConfigData("oasisdesert") then
        table.insert(task_set.tasks, "OasisDesert")
    end

    if GetModConfigData("bananameadow") then
        table.insert(task_set.tasks, "BananaMeadow")
    end

    if GetModConfigData("moonquaybeach") then
        table.insert(task_set.tasks, "MoonQuayBeach")
    end
end)
